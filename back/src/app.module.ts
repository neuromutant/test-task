import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MetricsModule } from './application/Metrics/modules/metrics.module';
import { UsersModule } from './application/Users/modules/users.app.module';
import { dbConfig } from './configs/db.config';
import { Metric } from './domains/Metrics/metric.domain.entity';
import { User } from './domains/Users/user.domain.entity';

@Module({
  imports: [
    UsersModule,
    MetricsModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: dbConfig.POSTGRES_PORT,
      username: dbConfig.POSTGRES_USER,
      password: dbConfig.POSTGRES_PASSWORD,
      database: dbConfig.POSTGRES_DB,
      entities: [User, Metric],
      // I know this shouldn't be used in production enviorment
      // usually a migration is created like this:
      // npx typeorm migration:create -n MigrationName -d MigrationPath
      // and after that applied to the db
      synchronize: true,
    }),
  ],
})
export class AppModule {}
