import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class User {
  constructor(params: {
    id: number;
    firstName: string;
    lastName: string;
    avatar: string;
    email: string;
  }) {
    Object.assign(this, params || {});
  }

  // not using primary generated column since all of our ids are external
  @PrimaryColumn()
  id: number;

  @Column('varchar')
  firstName: string;

  @Column('varchar')
  lastName: string;

  @Column('varchar')
  avatar: string;

  @Column('varchar')
  email: string;
}
