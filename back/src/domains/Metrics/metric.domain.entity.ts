import { Entity, Column, PrimaryColumn } from 'typeorm';
export interface IMetric {
  CR: number;
  EPC: number;
  apprComm: number;
  apprLeadCount: number;
  apprSaleAmount: number;
  apprSaleCount: number;
  bonusComm: number;
  bonusCount: number;
  clickCount: number;
  commClickCount: number;
  date: Date;
  dispComm: number;
  dispLeadCount: number;
  dispSaleAmount: number;
  dispSaleCount: number;
  grossComm: number;
  grossLeadCount: number;
  grossSaleAmount: number;
  grossSaleCount: number;
  impCount: number;
  netComm: number;
  netLeadCount: number;
  netSaleAmount: number;
  netSaleCount: number;
  pendComm: number;
  pendLeadCount: number;
  pendSaleAmount: number;
  pendSaleCount: number;
  totalComm: number;
  origin_date: Date;
}

@Entity()
export class Metric {
  constructor(params: IMetric) {
    Object.assign(this, params || {});
  }
  // using the simplest solution here
  // there should be an auto incrementable id
  // and relationships to user/company etc
  @PrimaryColumn()
  public date: Date;
  @Column('double precision')
  CR: number;
  @Column('double precision')
  EPC: number;
  @Column('double precision')
  apprComm: number;
  @Column('bigint')
  apprLeadCount: number;
  @Column('double precision')
  apprSaleAmount: number;
  @Column('bigint')
  apprSaleCount: number;
  @Column('double precision')
  bonusComm: number;
  @Column('bigint')
  bonusCount: number;
  @Column('bigint')
  clickCount: number;
  @Column('bigint')
  commClickCount: number;
  @Column('double precision')
  dispComm: number;
  @Column('bigint')
  dispLeadCount: number;
  @Column('double precision')
  dispSaleAmount: number;
  @Column('bigint')
  dispSaleCount: number;
  @Column('double precision')
  grossComm: number;
  @Column('bigint')
  grossLeadCount: number;
  @Column('double precision')
  grossSaleAmount: number;
  @Column('bigint')
  grossSaleCount: number;
  @Column('bigint')
  impCount: number;
  @Column('double precision')
  netComm: number;
  @Column('bigint')
  netLeadCount: number;
  @Column('double precision')
  netSaleAmount: number;
  @Column('bigint')
  netSaleCount: number;
  @Column('double precision')
  pendComm: number;
  @Column('bigint')
  pendLeadCount: number;
  @Column('double precision')
  pendSaleAmount: number;
  @Column('bigint')
  pendSaleCount: number;
  @Column('double precision')
  totalComm: number;
}
