import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Metric } from './metric.domain.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Metric])],
  exports: [TypeOrmModule],
})
export class MetricsDbModule {}
