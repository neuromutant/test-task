import puppeteer from 'puppeteer';

import { Injectable } from '@nestjs/common';
import { metricsConfig } from 'src/configs/metrics.config';
import _chunk from 'lodash.chunk';
import parse from 'date-fns/parse';
import en from 'date-fns/locale/en-US';
import { range } from 'lodash';
import { expectationFailedExceptionFactory } from 'src/application/common/exceptions/exceptions';
import { IMetric } from 'src/domains/Metrics/metric.domain.entity';

// the best way to parse the table in this case is to use api
// or downloading it as excel and parsing the file itself
// or if the browser usage is a must to bypass something
// then requests interceptors should be used.
// I know that it is better to use xpath for selectors instead of css selectors, but
// it doesn't look like any of xpath implemented except for $x (for example playwright handles this stuff better)
// I understand that I could select all the table entries from the menu, but I think it's intended to go through pagination.
// also it could look much better, but a test is a test :)

@Injectable()
export class MetricsPuppeteerService {
  private browser: puppeteer.Browser;
  private page: puppeteer.Page;
  rangeFrom = '10/01/2020';
  rangeTo = '10/30/2020';

  private async createBrowserInstance(url: string): Promise<void> {
    this.browser = await puppeteer.launch({
      headless: metricsConfig.METRICS_BROWSER_IS_HEADLESS,
      slowMo: metricsConfig.METRICS_BROWSER_SLOWDOWN_TIME,
    });
    this.page = await this.browser.newPage();
    await this.page.goto(url);
  }

  public async parse(): Promise<Array<IMetric[]>> {
    // this can be absracted further to something like
    // this.asyncTask(this.createBrowserInstance, metricsConfig.DASHBOARD_LOGIN_URL)
    // (error factory, custom error message)
    // but it would require some time to find proper way to use it
    // in a class without inheritance or binding
    // maybe it could be in it's own service, but it's
    // out of scope of this task

    try {
      await this.createBrowserInstance(metricsConfig.DASHBOARD_LOGIN_URL);
    } catch (error) {
      await this.browser.close();
      expectationFailedExceptionFactory(
        `Browser creation failed with error ${error}`,
      );
    }

    try {
      await this.loginToDashboard();
    } catch (error) {
      await this.browser.close();
      expectationFailedExceptionFactory(
        `Login to dashboard failed with error ${error}`,
      );
    }

    try {
      await this.goToMetricsPageAndSetPrefs();
    } catch (error) {
      await this.browser.close();
      expectationFailedExceptionFactory(
        `Metrics page failed with error ${error}`,
      );
    }

    try {
      await this.selectAllColumns();
      const result = await this.parseTable();
      await this.browser.close();
      return result;
    } catch (error) {
      await this.browser.close();
      expectationFailedExceptionFactory(
        `Table parser failed with error ${error}`,
      );
    }
  }

  private async goToLastPageAndGetPageNum() {
    await this.page.click(
      '#DataTables_Table_0_wrapper > div:nth-child(3) > div.col-md-8.col-sm-12 > div.dataTables_paginate.paging_bootstrap_full_number > ul > li:nth-child(9)',
    );
    await this.page.waitForTimeout(3000);
    const num = this.page.$eval(
      '#DataTables_Table_0_wrapper > div:nth-child(3) > div.col-md-8.col-sm-12 > div.dataTables_paginate.paging_bootstrap_full_number > ul > li.active > a',
      (elem) => ~~elem.textContent,
    );
    return num;
  }

  // for now all the stuff needed for the script are stored in env instead
  // also things like selector paths can be stored in the db
  private async loginToDashboard() {
    await this.page.waitForSelector('form');

    await this.fillInput(
      'input[type="email"]',
      metricsConfig.DASHBOARD_USERNAME,
    );

    await this.fillInput(
      'input[type="password"]',
      metricsConfig.DASHBOARD_PASSWORD,
    );

    await this.page.click('button[type="submit"]');
    await this.page.waitForTimeout(1000);
  }

  public async goToMetricsPageAndSetPrefs() {
    await this.page.goto(metricsConfig.DASHBOARD_METRICS_URL, {
      waitUntil: 'domcontentloaded',
    });

    await this.removeElem('#beamerPushModal');
    await this.clickBtn('#dashboard-report-range');

    await this.page.waitForSelector('.daterangepicker');
    await this.fillInput('input[name="daterangepicker_start"]', this.rangeFrom);
    await this.fillInput('input[name="daterangepicker_end"]', this.rangeTo);

    await this.clickBtn(
      '#drp_global > div > button.applyBtn.btn.btn-sm.btn-success',
    );
  }

  private async selectAllColumns() {
    await this.page.waitForTimeout(3000);
    await this.clickBtn(
      'a.btn.btn-circle.btn-sm.btn-default.table-tools.tooltips',
    );
    await this.page.$eval(
      '#setupColumns2 > div > div > div.modal-body > div > div.right-list > div.slimScrollDiv > div.dd.list-scroller.scroller > ol',
      (el) => {
        const children = el.children;
        [...children].forEach((li) => {
          const btn = li.querySelector('button');
          btn.click();
        });
      },
    );
    await this.page.$eval(
      '#setupColumns2 > div > div > div.modal-footer > button.btn.green',
      (el: HTMLButtonElement) => {
        el.click();
      },
    );
  }

  // it would better be in a separate service/helper for table parsing
  // with different scenarios for different tables
  // also returning all the result, but paginating would be better
  private async parseTable() {
    await this.page.waitForTimeout(3000);
    const lastPage = await this.goToLastPageAndGetPageNum();
    const headers = await this.getHeaders();
    const result = [await this.parseTablePage(headers)];

    for (const i of range(1, lastPage)) {
      await this.page.click(
        '#DataTables_Table_0_wrapper > div:nth-child(3) > div.col-md-8.col-sm-12 > div.dataTables_paginate.paging_bootstrap_full_number > ul > li:nth-child(2) > a',
      );
      const pageData = await this.parseTablePage(headers);
      result.push(pageData);
    }
    return result;
  }

  private async parseTablePage(headers) {
    await this.page.waitForTimeout(3000);
    const tableData = await this.getTableData();
    const chunked = _chunk(tableData, headers.length);
    const objects = chunked.map((chunk) =>
      Object.fromEntries(
        chunk.map((item, i) => [headers[i], this.processTableText(item)]),
      ),
    );
    return objects;
  }

  private async getHeaders() {
    const headers = await this.page.$eval(
      '#DataTables_Table_0 > thead > tr.heading',
      (el) => {
        const children = el.children;
        return [...children].map((th) => th.getAttribute('data-data'));
      },
    );
    return headers;
  }

  private async getTableData() {
    const rows = await this.page.$eval(
      '#DataTables_Table_0 > tbody',
      (tableBody) => {
        const tds = tableBody.getElementsByTagName('td');
        return [...tds].map((td) => td.innerText);
      },
    );
    return rows;
  }

  // service that also handles the amount of launched browsers, tabs, etc
  private async clearInput(selector) {
    await this.page.$eval(selector, (el: HTMLInputElement) => (el.value = ''));
  }
  private async fillInput(selector, value) {
    await this.page.waitForSelector(selector);
    await this.clearInput(selector);
    await this.page.focus(selector);
    await this.page.keyboard.type(value);
  }
  private async clickBtn(selector, timeout = 10000) {
    await this.page.waitForSelector(selector, { timeout });
    await this.page.click(selector);
  }
  private async removeElem(selector, timeout = 10000) {
    try {
      await this.page.waitForSelector(selector, {
        timeout,
      });
      await this.page.evaluate((selector) => {
        const popup = document.querySelector(selector);
        popup.parentNode.removeChild(popup);
      }, selector);
    } catch (error) {}
  }
  private processTableText(text) {
    text = text.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');

    // process float
    if (!isNaN(text) && text.toString().indexOf('.') != -1) {
      return parseFloat(text);
    }
    // process number
    else if (!isNaN(text)) {
      return ~~text;
    }

    // process date
    else {
      return parse(text, 'MMMM dd yyyy', new Date(), {
        locale: en,
      });
    }
  }
}
