import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginateQuery } from 'src/application/common/helpers/paginate';
import { Metric } from 'src/domains/Metrics/metric.domain.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MetricsRepo {
  constructor(
    @InjectRepository(Metric)
    private metricsRepository: Repository<Metric>,
  ) {}

  async createManyMetrics(metrics: Metric[]) {
    await this.metricsRepository
      .createQueryBuilder()
      .insert()
      .values(metrics)
      .orIgnore()
      .execute();
  }
  async findAll(page, size): Promise<{ data: Metric[]; total: number }> {
    const [data, total] = await this.metricsRepository.findAndCount({
      order: { date: 'ASC' },
      ...paginateQuery(page, size),
    });
    return { data, total };
  }
}
