import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { IsNumber, NotEquals } from 'class-validator';
import {
  IPaged,
  paginateResponse,
} from 'src/application/common/helpers/paginate';
import { MetricsRepo } from '../repos/metrics.repo';
import { MetricVm } from '../vms/metric.vm';

export class GetAllMetricsPaginatedQuery {
  @IsNumber()
  @NotEquals(0)
  readonly page: number;
  @IsNumber()
  @NotEquals(0)
  readonly size: number;

  constructor(page, size) {
    this.page = page;
    this.size = size;
  }
}

@QueryHandler(GetAllMetricsPaginatedQuery)
export class GetAllMetricsPaginatedQueryHandler
  implements IQueryHandler<GetAllMetricsPaginatedQuery>
{
  constructor(private repository: MetricsRepo) {}

  async execute(query: GetAllMetricsPaginatedQuery): Promise<IPaged<MetricVm>> {
    const metrics = await this.repository.findAll(query.page, query.size);
    const mappedMetrics = metrics.data.map((user) => new MetricVm(user));
    return paginateResponse<MetricVm>(
      mappedMetrics,
      query.page,
      metrics.total,
      query.size,
    );
  }
}
