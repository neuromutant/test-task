import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { IPaged } from 'src/application/common/helpers/paginate';
import { UserVm } from 'src/application/Users/vms/user.vm';
import { ParseMetricsCmd } from '../cmds/parseMetrics.cmd';
import { GetAllMetricsPaginatedQuery } from '../queries/getAllMetricsPaginated.query';

@Controller('metrics')
export class MetricsController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  // this controller should add some task to queue
  @Post('parse')
  async parseDataTable() {
    const cmd = new ParseMetricsCmd();
    const res = await this.commandBus.execute(cmd);
    return res;
  }

  @Post('paged')
  @HttpCode(200)
  async getPaged(
    @Body() query: GetAllMetricsPaginatedQuery,
  ): Promise<IPaged<UserVm[]>> {
    const result = await this.queryBus.execute(
      new GetAllMetricsPaginatedQuery(query.page, query.size),
    );
    return result;
  }
}
