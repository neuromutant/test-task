import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { IMetric, Metric } from 'src/domains/Metrics/metric.domain.entity';
import { MetricsRepo } from '../repos/metrics.repo';
import { MetricsPuppeteerService } from '../services/metrics.puppeteer.service';

// this should be used in a cron job that parses tasks from db and
// runs them
// not in a api request from user, since there can be timeouts and unexpected
// behaviour. Or it could be something that is put in some kind of queue
// by user and then we can notify user via sockets or long polling
// but for test task it is as it is :)

export class ParseMetricsCmd {}

@CommandHandler(ParseMetricsCmd)
export class ParseMetricsCmdHandler
  implements ICommandHandler<ParseMetricsCmd>
{
  constructor(
    private repository: MetricsRepo,
    private tableParserService: MetricsPuppeteerService,
  ) {}

  async execute(cmd: ParseMetricsCmd): Promise<void> {
    const result = await this.tableParserService.parse();
    const flatResult = result.flatMap((page) => page);
    await this.repository.createManyMetrics(flatResult);
  }

  // not pretty, should be used with some kind of
  // mapping library
  mapMetrics(parsedMetrics: IMetric[]): Metric[] {
    return parsedMetrics.map(
      (dto) =>
        new Metric({
          CR: dto.CR,
          EPC: dto.EPC,
          apprComm: dto.apprComm,
          apprLeadCount: dto.apprLeadCount,
          apprSaleAmount: dto.apprSaleAmount,
          apprSaleCount: dto.apprSaleCount,
          bonusComm: dto.bonusComm,
          bonusCount: dto.bonusCount,
          clickCount: dto.clickCount,
          commClickCount: dto.commClickCount,
          date: dto.date,
          dispComm: dto.dispComm,
          dispLeadCount: dto.dispLeadCount,
          dispSaleAmount: dto.dispSaleAmount,
          dispSaleCount: dto.dispSaleCount,
          grossComm: dto.grossComm,
          grossLeadCount: dto.grossLeadCount,
          grossSaleAmount: dto.grossSaleAmount,
          grossSaleCount: dto.grossSaleCount,
          impCount: dto.impCount,
          netComm: dto.netComm,
          netLeadCount: dto.netLeadCount,
          netSaleAmount: dto.netSaleAmount,
          netSaleCount: dto.netSaleCount,
          pendComm: dto.pendComm,
          pendLeadCount: dto.pendLeadCount,
          pendSaleAmount: dto.pendSaleAmount,
          pendSaleCount: dto.pendSaleCount,
          totalComm: dto.totalComm,
          origin_date: dto.origin_date,
        }),
    );
  }
}
