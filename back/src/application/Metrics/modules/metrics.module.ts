import { Module } from '@nestjs/common';
import { MetricsDbModule } from 'src/domains/Metrics/metric.domain.module';
import { MetricsController } from '../controllers/metrics.controller';
import { MetricsRepo } from '../repos/metrics.repo';
import { MetricsPuppeteerService } from '../services/metrics.puppeteer.service';
import { ParseMetricsCmdHandler } from '../cmds/metrics.cmds';
import { GetAllMetricsPaginatedQueryHandler } from '../queries/metrics.queries';
import { CqrsModule } from '@nestjs/cqrs';
@Module({
  imports: [CqrsModule, MetricsDbModule],
  providers: [
    MetricsPuppeteerService,
    ParseMetricsCmdHandler,
    GetAllMetricsPaginatedQueryHandler,
    MetricsRepo,
  ],
  controllers: [MetricsController],
})
export class MetricsModule {}
