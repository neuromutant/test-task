export interface IReqResUserApiResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: IReqResUserDto[];
}

export interface IReqResUserDto {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
