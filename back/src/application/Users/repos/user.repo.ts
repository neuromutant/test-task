import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginateQuery } from 'src/application/common/helpers/paginate';
import { Repository } from 'typeorm';
import { User } from '../../../domains/Users/user.domain.entity';

@Injectable()
export class UsersRepo {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async createUser(user: User): Promise<number> {
    const createdUser = await this.usersRepository.insert(user);
    return createdUser.raw.insertId;
  }

  async createManyUsers(users: User[]) {
    await this.usersRepository
      .createQueryBuilder()
      .insert()
      .values(users)
      .orIgnore()
      .execute();
  }

  async updateUser(user: User): Promise<number> {
    await this.usersRepository.save(user);
    return user.id;
  }

  async findAll(page, size): Promise<{ data: User[]; total: number }> {
    const [data, total] = await this.usersRepository.findAndCount({
      // custom sorting cound be added,
      order: { id: 'ASC' },
      ...paginateQuery(page, size),
    });
    return { data, total };
  }

  findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }
}
