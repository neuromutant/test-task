import { Controller, Param, Get, Post, Body, HttpCode } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import {
  GetUserByIdQuery,
  GetAllUsersPagedQuery,
} from 'src/application/Users/queries/users.queries';
import { FetchUsersCmd } from 'src/application/Users/cmds/users.cmds';
import { User } from 'src/domains/Users/user.domain.entity';
import { IPaged } from 'src/application/common/helpers/paginate';
import { UserVm } from '../vms/user.vm';

@Controller('users')
export class UsersController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get(':id')
  async getById(@Param('id') id: number): Promise<User> {
    const query = new GetUserByIdQuery(id);
    const result = await this.queryBus.execute(query);
    return result;
  }

  @Post('paged')
  @HttpCode(200)
  // simple way to use one query/cmd for validation and handling with nest
  // since it's serialized and class-validator context is lost
  // a dto can be used here instead, but it looks like an overkill for this task
  // 200 is used since something on consumer part may expect 200 as usual, not 201
  // also post is used for simplicity, it should be a get request with query params
  async getPaged(
    @Body() query: GetAllUsersPagedQuery,
  ): Promise<IPaged<UserVm[]>> {
    const result = await this.queryBus.execute(
      new GetAllUsersPagedQuery(query.page, query.size),
    );
    return result;
  }

  @Post('fetch')
  async fetchUsersFromRemoteApi() {
    const cmd = new FetchUsersCmd();
    const result = await this.commandBus.execute(cmd);
    return result;
  }
}
