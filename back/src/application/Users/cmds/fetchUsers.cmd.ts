import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { User } from '../../../domains/Users/user.domain.entity';
import { UsersRepo } from '../repos/user.repo';
import { UserFetchService } from '../services/userFetch.service';
import { IReqResUserDto } from '../types/reqResUserApi.type';
import range from 'lodash.range';

export class FetchUsersCmd {}

@CommandHandler(FetchUsersCmd)
export class FetchUsersCmdHandler implements ICommandHandler<FetchUsersCmd> {
  constructor(
    private repository: UsersRepo,
    private userFetchService: UserFetchService,
  ) {}

  async execute(cmd: FetchUsersCmd): Promise<void> {
    const totalPages = await this.handleFirstPage();
    //getting all pages here in one loop, though chunking would be better
    const fetchPromises = range(2, totalPages + 1).map((page) =>
      this.userFetchService.getUsersFromReqres(page),
    );
    const result = await Promise.all(fetchPromises);
    const mappedUsers = result.flatMap((res) => this.mapUsers(res.data));
    await this.repository.createManyUsers(mappedUsers);
  }

  async handleFirstPage(): Promise<number> {
    const firstPage = await this.userFetchService.getUsersFromReqres(1);
    const mappedUsers = this.mapUsers(firstPage.data);
    await this.repository.createManyUsers(mappedUsers);
    return firstPage.total_pages;
  }

  mapUsers(apiUsers: IReqResUserDto[]): User[] {
    return apiUsers.map(
      (dto) =>
        new User({
          avatar: dto.avatar,
          email: dto.email,
          firstName: dto.first_name,
          lastName: dto.last_name,
          id: dto.id,
        }),
    );
  }
}
