import { NotFoundException } from '@nestjs/common';
import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { IsNumber, NotEquals } from 'class-validator';
import { UsersRepo } from '../repos/user.repo';
import { UserVm } from '../vms/user.vm';

export class GetUserByIdQuery {
  @NotEquals(0)
  @IsNumber()
  readonly id: number;
  constructor(id: number) {
    this.id = id;
  }
}

@QueryHandler(GetUserByIdQuery)
export class GetUserByIdQueryHandler
  implements IQueryHandler<GetUserByIdQuery>
{
  constructor(private repository: UsersRepo) {}

  async execute(query: GetUserByIdQuery): Promise<UserVm> {
    const foundUser = await this.repository.findOne(query.id);
    if (!!foundUser) {
      throw new NotFoundException(`User with id ${query.id} not found`);
    }
    return new UserVm(foundUser);
  }
}
