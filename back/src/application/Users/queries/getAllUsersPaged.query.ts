import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { IsNumber, NotEquals } from 'class-validator';
import {
  IPaged,
  paginateResponse,
} from 'src/application/common/helpers/paginate';
import { UsersRepo } from '../repos/user.repo';
import { UserVm } from '../vms/user.vm';

export class GetAllUsersPagedQuery {
  @IsNumber()
  @NotEquals(0)
  readonly page: number;
  @IsNumber()
  @NotEquals(0)
  readonly size: number;

  constructor(page, size) {
    this.page = page;
    this.size = size;
  }
}

@QueryHandler(GetAllUsersPagedQuery)
export class GetAllUsersPagedQueryHandler
  implements IQueryHandler<GetAllUsersPagedQuery>
{
  constructor(private repository: UsersRepo) {}

  async execute(query: GetAllUsersPagedQuery): Promise<IPaged<UserVm>> {
    const foundUsers = await this.repository.findAll(query.page, query.size);
    const mappedUsers = foundUsers.data.map((user) => new UserVm(user));
    return paginateResponse<UserVm>(
      mappedUsers,
      query.page,
      foundUsers.total,
      query.size,
    );
  }
}
