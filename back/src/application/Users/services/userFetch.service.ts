import { Injectable } from '@nestjs/common';
import * as axios from 'axios';
import { expectationFailedExceptionFactory } from 'src/application/common/exceptions/exceptions';
import { promiseRetry } from 'src/application/common/helpers/promiseRetry';
import { IReqResUserApiResponse } from '../types/reqResUserApi.type';

@Injectable()
export class UserFetchService {
  instance: axios.AxiosInstance;
  constructor() {
    this.instance = axios.default.create({
      baseURL: 'https://reqres.in/api',
    });
  }
  async getUsersFromReqres(page: number): Promise<IReqResUserApiResponse> {
    // simple retry
    try {
      const users = await promiseRetry(
        () => this.instance.get('/users', { params: { page } }),
        5,
        1000,
      );
      return users.data as IReqResUserApiResponse;
    } catch (error) {
      expectationFailedExceptionFactory(error);
    }
  }
}
