// a package for automapping should be used, but it would take some time so
// I used this simple class

import { User } from 'src/domains/Users/user.domain.entity';

export class UserVm {
  id: number;
  firstName: string;
  lastName: string;
  avatar: string;
  email: string;
  constructor(user: User) {
    Object.assign(this, user);
  }
}
