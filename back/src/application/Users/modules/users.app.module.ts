import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { UsersRepo } from 'src/application/Users/repos/user.repo';
import { UsersDbModule } from 'src/domains/Users/user.domain.module';
import { FetchUsersCmdHandler } from '../cmds/users.cmds';
import { UsersController } from '../controllers/users.controller';
import {
  GetAllUsersPagedQueryHandler,
  GetUserByIdQueryHandler,
} from '../queries/users.queries';
import { UserFetchService } from '../services/userFetch.service';
@Module({
  imports: [CqrsModule, UsersDbModule],
  providers: [
    UserFetchService,
    UsersRepo,
    //Cmd
    FetchUsersCmdHandler,
    // Query
    GetAllUsersPagedQueryHandler,
    GetUserByIdQueryHandler,
  ],
  controllers: [UsersController],
})
export class UsersModule {}
