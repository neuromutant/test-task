import { HttpException, HttpStatus } from '@nestjs/common';

export const expectationFailedExceptionFactory = (error) => {
  throw new HttpException(
    {
      status: HttpStatus.EXPECTATION_FAILED,
      // error should be sanitized before returning since it can contain sensitive data
      error,
    },
    HttpStatus.EXPECTATION_FAILED,
  );
};
