export interface IPaged<T> {
  totalPages: number;
  totalCount: number;
  size: number;
  currentPage: number;
  data: T[];
}

export const paginateResponse = <T>(
  data: T[],
  currentPage,
  totalCount,
  size,
) => {
  const totalPages = ~~(totalCount / size) + (totalCount % size > 0 ? 1 : 0);
  return {
    totalPages,
    totalCount,
    currentPage,
    size,
    data,
  } as IPaged<T>;
};

export const paginateQuery = (page: number, size: number) => ({
  skip: (page - 1) * size,
  take: size,
});
