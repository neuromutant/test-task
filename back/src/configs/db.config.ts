export const dbConfig = {
  POSTGRES_DB: process.env['POSTGRES_DB'],
  POSTGRES_USER: process.env['POSTGRES_USER'],
  POSTGRES_PASSWORD: process.env['POSTGRES_PASSWORD'],
  POSTGRES_PORT: ~~process.env['POSTGRES_PORT'],
  PG_DATA: process.env['PG_DATA'],
};
