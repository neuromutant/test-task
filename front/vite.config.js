const {generate_vite_paths, generate_config_files} = require('./config/configGen');
require('dotenv').config()
import { createVuePlugin } from 'vite-plugin-vue2'
generate_config_files();
export default {
	plugins: [
		createVuePlugin(),
	],
	base: '/admin/',
	server: {
		port: process.env['VITE_PORT'],
		proxy: {
			'^/api':{
				target:process.env['VITE_BACKEND_URL'],
				rewrite: (path) => path.replace(/^\/api/, '')
			}
		},
	},
	resolve: {
		extensions: ['.js', '.vue', '.json', '.ts'],
		alias: [...generate_vite_paths()],
	},
    module:{
            test: /\.vue$/,
            loader: 'vue-loader'
    },
	build: {
		outDir: './dist',
	}
}
