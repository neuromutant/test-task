
import {RouteConfig}         from 'vue-router';

export enum UserRouteNames {
	Users = 'Users',
	UsersEdit = 'UsersEdit',
}

export const UserRoutes: Array<RouteConfig> = [
    {
        path     : '/users',
        name     : 'Users',
        component: () => import('@entities/Users/views/UsersList.vue'),
        meta     : {
            icon    : 'mdi-account-group',
            menuName: 'Users',
            menuPath: '/users',
        },
    }
];
