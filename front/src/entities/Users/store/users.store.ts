import { set }   from '@/plugins/vuex-mutations';
import {api}     from '@api';

const emptyUser = (overrides?) => ({
    id       : 0,
    firstName: '',
    lastName : '',
    avatar   : '',
    email    : '',
    ...overrides,
});

const emptyPagination = (overrides?) => ({
    totalPages : 0,
    totalCount : 0,
    size       : 10,
    currentPage: 1,
    ...overrides,
});

export const usersStore = {
    namespaced: true,
    state     : {
        pagination: emptyPagination(),
        current   : emptyUser(),
        list      : [],
    },
    mutations: {
        SET_CURRENT               : set('current'),
        SET_LIST                  : set('list'),
        SET_PAGINATION_TOTAL_PAGES: set('pagination.totalPages'),
        SET_PAGINATION_TOTAL_COUNT: set('pagination.totalCount'),
        SET_PAGINATION_SIZE       : set('pagination.size'),
        SET_PAGINATION_PAGE       : set('pagination.currentPage'),
    },
    actions: {
        async preloadUsers() {
            await api.users.fetchUsers();
        },
        async getUsersList({state, commit}) {
            const {currentPage, size} = state.pagination;
            const paginatedUsers      = await api.users.getPagedUsers(currentPage, size);
            
            const {totalPages, totalCount, data} = paginatedUsers;
            commit('SET_PAGINATION_TOTAL_COUNT', totalCount);
            commit('SET_PAGINATION_TOTAL_PAGES', totalPages);
            commit('SET_LIST', data);
        },
        async getUserById({commit}, id:number) {
            const user = await api.users.getUserById(id);
            commit('SET_CURRENT', user);
        },
    },
    // I usually don't use getters at all, since they are usually are a source of problems
    // when working in big teams
};

export const usersStoreModels = {
    // underscore in name is the dot analog
    pagination_size      : ['pagination.size', 'SET_PAGINATION_SIZE'],
    pagination_page      : ['pagination.page', 'SET_PAGINATION_PAGE'],
    //readonly
    pagination_totalCount: ['pagination.totalCount'],
};

export const usersStorePath = 'users';