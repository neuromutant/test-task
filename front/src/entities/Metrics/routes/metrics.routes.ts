
import {RouteConfig}         from 'vue-router';

export enum MetricsRouteNames {
	Metrics = 'Metrics',
	MetricsEdit = 'MetricsEdit',
}

export const MetricsRoutes: Array<RouteConfig> = [
    {
        path     : '/metrics',
        name     : 'Metrics',
        component: () => import('@entities/Metrics/views/MetricsList.vue'),
        meta     : {
            icon    : 'mdi-chart-areaspline',
            menuName: 'Metrics',
            menuPath: '/metrics',
        },
    }
];
