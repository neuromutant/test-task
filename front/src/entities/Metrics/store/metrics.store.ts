import { set }   from '@/plugins/vuex-mutations';
import {api}     from '@api';

const emptyMetric = (overrides?) => ({
    id       : 0,
    firstName: '',
    lastName : '',
    avatar   : '',
    email    : '',
    ...overrides,
});

const emptyPagination = (overrides?) => ({
    totalPages : 0,
    totalCount : 0,
    size       : 10,
    currentPage: 1,
    ...overrides,
});

export const metricsStore = {
    namespaced: true,
    state     : {
        pagination: emptyPagination(),
        current   : emptyMetric(),
        list      : [],
    },
    mutations: {
        SET_CURRENT               : set('current'),
        SET_LIST                  : set('list'),
        SET_PAGINATION_TOTAL_PAGES: set('pagination.totalPages'),
        SET_PAGINATION_TOTAL_COUNT: set('pagination.totalCount'),
        SET_PAGINATION_SIZE       : set('pagination.size'),
        SET_PAGINATION_PAGE       : set('pagination.currentPage'),
    },
    actions: {
        async parseMetrics() {
            await api.metrics.parseMetrics();
        },
        async getMetricsList({state, commit}) {
            const {currentPage, size} = state.pagination;
            const paginatedMetrics    = await api.metrics.getPagedMetrics(currentPage, size);
            
            const {totalPages, totalCount, data} = paginatedMetrics;
            commit('SET_PAGINATION_TOTAL_COUNT', totalCount);
            commit('SET_PAGINATION_TOTAL_PAGES', totalPages);
            commit('SET_LIST', data);
        },
    },
};

export const metricsStoreModels = {
    pagination_size      : ['pagination.size', 'SET_PAGINATION_SIZE'],
    pagination_page      : ['pagination.page', 'SET_PAGINATION_PAGE'],
    pagination_totalCount: ['pagination.totalCount'],
};

export const metricsStorePath = 'metrics';