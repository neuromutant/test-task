export interface IUserVm {
    id: number;
    firstName: string;
    lastName: string;
    avatar: string;
    email: string;
}