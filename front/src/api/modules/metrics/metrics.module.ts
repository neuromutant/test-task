// this file should be autogenerated,
// first swagger schema can be generated for nest,
// then swagger schema can be transformed to typescript client
// via nswag

import { IPaged }          from '@/api/common/types';
import { axiosInstance }   from '../../axiosInstance';
import { IMetricVm }       from './metrics.types';

const apiEndpoint = '/metrics';

export const metricsApiModule = {
    async parseMetrics(): Promise<void> {
        await axiosInstance.post(`${apiEndpoint}/parse`);
    },
    async getPagedMetrics(page, size): Promise<IPaged<IMetricVm[]>> {
        return (await (axiosInstance.post(`${apiEndpoint}/paged`, {page, size}))).data;
    },
};