export interface IPaged<T> {
    totalPages: number;
    totalCount: number;
    size: number;
    currentPage: number;
    data: T[];
  }