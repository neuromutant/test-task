import { metricsApiModule } from './modules/metrics/metrics.module';
import { usersApiModule }   from './modules/users/users.module';

export const api = {
    users  : usersApiModule,
    metrics: metricsApiModule,
};
