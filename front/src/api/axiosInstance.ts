import axios                   from 'axios';
import {eventHub}              from '@/eventhub';

export const axiosInstance = axios.create({
    baseURL: '/api',
});

axiosInstance.interceptors.response.use(
    response => {
        eventHub.$emit('after-response');
        return response;
    },
    error => {
        eventHub.$emit('response-error');
        console.log('Axios response error', {error});
        return Promise.reject(error);
    }
);
