
import { metricsStore, metricsStorePath } from '@/entities/Metrics/store/metrics.store';
import { usersStore, usersStorePath }     from '@/entities/Users/store/users.store';
import Vue                                from 'vue';
import Vuex                               from 'vuex';

Vue.use(Vuex);

const store = {
    modules: {
        // Users
        [usersStorePath]  : usersStore,
        //Metrics
        [metricsStorePath]: metricsStore,
		
    },
};

export default new Vuex.Store(store);
