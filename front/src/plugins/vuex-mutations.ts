import _get                  from 'lodash.get';
import _set                  from 'lodash.set';
import Vue                   from 'vue';

//import { externalStateKeys } from '@/components/metaTable/keys/keys';

import {checker}             from './vuex-errors';

const before_hook = (type, name, ...args) => {
    // if (false) {
    // console.log(`before hook for ${name}`);
    // }
};

/**
* Установка значения по свойству объекта в стейте
* @param {string} prop имя в стейте (state[prop])
* @example
*   SET_PROPERTY:set('property')
*   commit('SET_PROPERTY', value);
*
*/
export const set = <T>(...props) => (state, val:T) => {
    const combined = props.join('.');
    before_hook('set', combined, val);
    checker('no_prop')({state, prop : combined});
    _set(state, combined, val);
};

/*export const set_current = <T>(prop) => (state, val:T) => {
	const combined_prop = `${externalStateKeys.current}.${prop}`;
	
	before_hook('set_current', combined_prop, val);
	checker('no_prop')({state, prop : combined_prop});
	_set(state, combined_prop, val);
};*/

export const set_number = <T>(prop) => (state, val:any) => {
    val = parseInt(val);
    before_hook('set_number', prop, val);
    checker('no_prop')({state, prop});
    _set(state, prop, val);
};

export const set_from_enum = <T>(prop, enumFrom) => (state, val:T) => {
    before_hook('set', prop, val);
    checker('no_prop')({state, prop});
    if (enumFrom[val]) {
        _set(state, prop, val);
    } else {
        throw new Error('Trying to set value that is not included in check enum');
    }
};

export const set_from_fun = (prop, factory) => (state, val) => {
    before_hook('set_from_fun', prop, val);
    checker('no_prop', 'not_function')({state, prop, factory});
    _set(state, prop, factory(val));
};

export const set_copy = prop => (state, val) => {
    _set(state, prop, JSON.parse(JSON.stringify(val)));
};

export const set_from_obj = (...stateProps) => (...objectProps) => (state, val) => {
    stateProps.forEach((prop,i) => {
        checker('no_prop', 'no_object_prop')({state, prop, objectProps, val, i});
        let objectVal = _get(val, objectProps[i]);
        if (objectVal === Object(objectVal)) {
            objectVal = {...objectVal};
        }
        _set(state, prop, objectVal);
    });
};

/**
* Object assign из объекта в стейт
* @param objs {Array<Object>} объекты, которые будут ассайгнуты до основного объекта)
* @param val {Object} объект, который будет ассайгнут последним при вызове коммита
* @example
*   ASSIGN_STATE:assign_state({hello:'world'}, {goodbye:'world'}, {my_new:'value'})
*   commit('SET_PROPERTY', myobj);
*
*/
export const assign_state = (...objs) => (state, val) => {
    before_hook('assign', objs , val);
    objs.forEach(o => Object.assign(state, o));
    Object.assign(state, val);
};
/**
* Устанавливает значение внутреннего объекта по указанному пути
* @param path {Sting} путь до объекта, например hello.world.one.two
* @param val {Any} значение, которое будет присвоено ключу обхекта
* @example
*   SET_DEEP_HELLO_WORLD:set_deep('hello.world')
*   commit('SET_DEEP_HELLO_WORLD', 'hello');
*
*/
export const set_deep = path => (state, val) => {
    before_hook('set_deep', path  , val);
    _set(state, path, val);
};

export const set_reactive = path => (state, val) => {
    Vue.set(state, path, val);
};

/**
* Устанавливает предустановленное значение внутреннего объекта по указанному пути
* @param path {Sting} путь до объекта, например hello.world.one.two
* @param val {Any} значение, которое будет присвоено ключу обхекта
* @example
*   SET_DEEP_HELLO_WORLD:set_deep_defined('hello.world', 'my default value')
*   commit('SET_DEEP_HELLO_WORLD');
*
*/
export const set_deep_defined = (path, value) => state => {
    before_hook('set_deep_defined', path, value);
    _set(state, path, value);
};

/**
* Установка значения по свойству объекта в стейте
* @param prop {string} имя в стейте (state[prop])
* @param val значение (state[prop] = val)
* @example
*   SET_PROPERTY:set('property', 'string value')
*   commit('SET_PROPERTY');
*
*/
export const set_defined = (prop, val, ...args) => state => {
    before_hook('set_defined', prop, val);
    checker('no_prop')({state, prop});
    if (typeof val === 'function') {
        _set(state, prop,  val(...args));
        return;
    }
    if (Array.isArray(val)) {
        _set(state, prop,  [...val]);
        return;
    }
    if (typeof val === 'object') {
        _set(state, prop,  {...val});
        return;
    }
    _set(state, prop, val);
};

/**
* Прибавляет число к числу в стейте
* @param prop {string} имя в стейте (state[prop])
* @param by {number} количество на которое уменьшаем (1 по умолчанию)
  @example
   INCREMENT_PROPERY:increment('property', 2);
   commit('INCREMENT_PROPERTY');
*/
export const increment = (prop, by = 1) => state => {
    before_hook('increment', prop, by);
    checker('no_prop', 'not_number')({state, prop, by});
    state[prop] += by;
};

/**
* Вычитает число из числа в стейте
* @param prop {string} имя в стейте (state[prop])
* @param by {number} количество на которое уменьшаем (1 по умолчанию)
  @example
   DECREMENT_PROPERY:decrement('property', 2);
   commit('DECREMENT_PROPERY');
*/
export const decrement = (prop, by = 1) => state => {
    before_hook('decrement', prop, by);
    checker('no_prop', 'not_number')({state, prop, by});
    state[prop] -= by;
};

/**
* Устанавливает значение свойства стейта на true
* @param prop {string} имя в стейте (state[prop])
  @example
   TO_TRUE_PROPERTY:to_true('property');
   commit('TO_TRUE_PROPERTY');
*/

export const to_true = prop => state => {
    before_hook('to_true', prop);
    checker('no_prop')({state, prop});
    state[prop] = true;
};

/**
*  Устанавлиевает значение свойства стейта на false
* @param prop {string} имя в стейте (state[prop])
  @example
   TO_FALSE_PROPERTY:to_false('property')
   commit('TO_FALSE_PROPERTY');
*/
export const to_false = prop => state => {
    before_hook('to_false', prop);
    checker('no_prop')({state, prop});
    state[prop] = false;
};
/**
*  Переключает значение свойства стейта
*  с true на false и наоборот
*  @param prop {string} имя в стейте (state[prop])
*  @example
*  TOGGLE_PROPERTY:toggle('property');
*  commit('TOGGLE_PROPERTY');
*/
export const toggle = prop => state => {
    before_hook('toggle', prop);
    checker('no_prop')({state, prop});
    state[prop] = !state[prop];
};

/**
*  Инициализирует значение свойства стейта
*  инициализация при помощи коструктора
*  @param prop {string} имя в стейте (state[prop])
*  @example
*   INIT_PROPERTY:init('property');
*   commit('INIT_PROPERTY');
*/
export const init = prop => state => {
    before_hook('toggle', prop);
    checker('no_prop')({state, prop});
    const type  = state[prop].constructor;
    state[prop] = new type();
};

/**
* Добавляет элемент в массив стейта по свойству
* @param prop имя в стейте (state[prop])
*   @example
*   PUSH_PROPERTY:push('property')
*   commit('PUSH_PROPERTY', value)
*/
export const push = prop => (state, val) => {
    before_hook('push', prop, val);
    checker('not_array', 'no_prop')({state, prop});
    state[prop].push(val);
};

/**
*  которая добавляет предустановленный
*  элемент в массив стейта по свойству
*  @param prop {string} имя в стейте (state[prop])
*  @param val значение того, что мы будем добавлять
*  @example
*   PUSH_DEFINED_PROPERTY:push_defined('property', 'my string');
*   commit('PUSH_DEFINED_PROPERTY');
*/
export const push_defined = (prop, val) => state => {
    before_hook('push', prop, val);
    checker('not_array', 'no_prop')({state, prop});
    state[prop].push(val);
};

/**
  Добавляет предустановленную
  фабрику и ее переменные по свойству в массив
  @param prop имя в стейте (state[prop])
  @param factory фабрика, которую будем передавать
  @param factory_prop переменные для фабрики, могут быть массивом
  @example
    const factory => (hello, world) => hello + world;
    PUSH_DEFINED_FACTORY_PROPERTY:push_defined_factory('property', factory, ['hello_string'], ['world_string'])
    commit('PUSH_DEFINED_FACTORY_PROPERTY')
*/
export const push_defined_factory = (prop, factory, factory_prop = []) => state => {
    before_hook('push_defined_factory', prop, factory, factory_prop);
    checker('not_function', 'not_array', 'no_prop')({state, prop, factory});
    state[prop].push(factory(...factory_prop));
};

/**
  Добавляет  фабрику при вызове функции и передачи в нее параметров.
  Параметры становятся параметрами фабри0ки и она добавляется в массив.
  @param prop {string} имя в стейте (state[prop])
  @param factory фабрика, которую будем передавать
  @example
    const factory => (hello, world) => hello + world;
    PUSH_DEFINED_FACTORY_PROPERTY:push_abs_factory('property', factory);
    commit('PUSH_DEFINED_FACTORY_PROPERTY', ['hello_string', 'world_string']);
*/
export const push_abs_factory = (prop, factory) => (state, val) => {
    before_hook('push_abs_factory', prop, factory, val);
    checker('not_function', 'not_array', 'no_prop')({state, prop, factory});
    state[prop].push(factory(...val));
};
/**
* Удаляет элемент из массива по индексу
* @param prop {string} имя в стейте (state[prop])
  @example
  SPLICE_PROPERTY: splice('property');
  commit('SPLICE_PROPERTY', 1);
*/
export const splice = prop => (state, index) => {
    before_hook('splice', prop, state, index);
    checker('no_prop', 'not_array', 'no_required_length')({state, prop, index});
    state[prop].splice(index, 1);
};
/**
  Удаляет найденный по значению элемент из массива
  @param prop {string} имя в стейте (state[prop])
  @example
  POP_PROPERTY: pop('property');
  commit('POP_PROPERTY', 'search value');
*/
export const pop = prop => (state, val) => {
    before_hook('pop', prop, val);
    checker('no_prop')({state, prop});
    const index = state[prop].indexOf(val);
    return index > -1  && state[prop].splice(index, 1);
};
/**
  Устанавливает свойство объекта в массиве стейта по айдишнику
  @param state_prop имя в стейте (state[prop])
  @param prop свойства в массиве объектов, которое будем менять
  @example
  SET_NAME_BY_ID: set_prop_by_id('users', 'name');
  commit('SET_NAME_BY_ID', {id:'123', value:'New Name'});
*/
export const set_prop_by_id = (state_prop, prop) => (state, obj) => {
    before_hook('set_prop_by_id', prop, state_prop, prop, obj);
    checker('no_prop')({state, prop});
    const {id, value} = obj;
    const found       = state[state_prop].find(item => item.id === id);
    checker('not_found', 'not_found_property')({found, state, name : state_prop});
    found[prop] = value;
};
/**
  Добавляет во вложенный массив объектов внутри массива в стейте
  state[свойство][массив][искомый объект][свойсто][массив]
  @param state_prop {string} имя в стейте (state[prop])
  @param prop {string} свойства в массиве объектов, которое будем менять
  @param _id название поля id
  @example
  PUSH_STREET_BY_USER_ID: push_by_id('users', 'streets');
  commit('PUSH_STREET_BY_USER_ID', {id:'123', value:'New Name'});
*/
export const push_by_id = (state_prop, prop, _id = 'id') => (state, obj) => {
    checker('no_prop')({state, prop});
    const {id, value} = obj;
    const found       = state[state_prop].find(x => x[_id] === id);
    checker('not_found', 'not_found_property')({found, state, prop, name : state_prop});
    found[prop].push(value);
};

/*
 Устанавливает всем объектам в массиве в указанном свойстве false
 @param state_prop {string} имя в стейте (state[prop])
 @param prop {string} свойства в массиве объектов, которое будем менять
 @example
 SET_ALL_ACTIVE_TO_FALSE:to_false_all('items', 'active');
 commit('SET_ALL_ACTIVE_TO_FALSE');
*/
export const to_false_all = (state_prop, prop) => state => {
    checker('no_prop')({state, prop : state_prop});
    for (const item of state[state_prop]) {
        item[prop] = false;
    }
};
/**
* Устанавливает всем объектам в массиве в указанном свойстве true
 * @param state_prop {string} имя в стейте (state[prop])
 * @param prop {string} свойства в массиве объектов, которое будем менять
 * @example
 * SET_ALL_ACTIVE_TO_TRUE:to_false_all('items', 'active');
 * commit('SET_ALL_ACTIVE_TO_TRUE');
*/
export const to_true_all = (state_prop, prop) => state => {
    before_hook('to_true_all', prop, state_prop, prop);
    checker('no_prop')({state, prop : state_prop});
    for (const item of state[state_prop]) {
        item[prop] = true;
    }
};

/**
* Добавляет объект в массив объектов если такого объекта в массиве нет
* сравнение происходит по свойствам target_prop и value_prop
 * @param state_array {string} путь до массива внутри стейта
 * @param target_prop {string} свойство с которым будем сравнивать объекты
 * @param value_prop {string} свойства принимаемого объекта, с которым будем сравнивать, по умолчанию равно target_prop
 * @example
 * TOGGLE:toggle_obj('users', 'id', 'id');
 * commit('TOGGLE', {id: 1, name:"Valera"});
*/
export const toggle_obj = (state_array, target_prop, value_prop = target_prop) => (state, val) => {
    before_hook('toggle_obj', state_array, target_prop, value_prop);
    checker('not_array', 'no_prop')({state, prop : state_array});
    const target = _get(state, state_array);
    const idx    = target.findIndex(item => item[target_prop] === val[value_prop]);
    if (idx >= 0) {
        return target.splice(idx, 1);
    } 
    target.push(val);
};

export const toggle_array = state_prop => (state, val) => {
    const state_arr = _get(state, state_prop);
    const idx       = state_arr.indexOf(val);
    if (idx >= 0) {
        state_arr.splice(idx, 1);
        return;
    }
    state_arr.push(val);
};