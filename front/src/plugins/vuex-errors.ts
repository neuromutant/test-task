import _has from 'lodash.has';

export const is_function = obj => {
    return !!(obj && obj.constructor && obj.call && obj.apply);
};

export const no_prop = ({state, prop}) => {
    if (!_has(state, prop)) {
        throw new Error(`no prop named ${prop} in vuex state`);
    }
    return true;
};

export const no_prop_deep = ({state, deep_prop}) => {
    if (!_has(state, deep_prop)) {
        throw new Error(`no deep_prop named ${deep_prop} in vuex state`);
    }
    return true;
};
export const not_array = ({state, prop}) => {
    if (!Array.isArray(state[prop])) {
        throw new Error(`trying to push to property named ${prop} that is not an Array`);
    }
    return true;
};

export const not_number = ({state, prop, by}) => {
    if (isNaN(state[prop])) {
        throw new Error(`trying to increment prop named ${prop} that is not a number`);
    }
    if (isNaN(by)) {
        throw new Error(`trying to increment prop named ${prop} with not a number ${by}`);
    }
    return true;
};

export const not_function = ({prop, factory}) => {
    if (!is_function(factory)) {
        throw new Error(`trying to push factory that is not a function to prop ${prop} `);
    }
    return true;
};

export const no_required_length = ({prop, state, index}) => {
    if (state[prop].length === 0) {
        throw new Error(`array ${prop} is 0 (zero) length`);
    }
    if (state[prop].length < index) {
        throw new Error(`array ${prop} is less than specified index of ${index}`);
    }
    return true;
};

export const not_found = ({name, found}) => {
    if (!found) {
        throw new Error(`result not found for ${name}`);
    }
    return true;
};
export const no_object_prop = ({objectProps, i, val}) => {
    const prop = objectProps[i];
    if (!prop && !_has(val, prop)) {
        throw new Error(`not found prop ${prop} in object ${objectProps}`);
    }
    return true;
};

export const not_found_property = ({name, found, property}) => {
    if (!found[property]) {
        throw new Error(`no property ${property} for found object for ${name}`);
    }
    return true;
};

export const checker_module = {
    no_object_prop,
    no_prop,
    not_array,
    not_number,
    not_function,
    no_required_length,
    not_found,
    not_found_property,
    no_prop_deep,
};

export const checker = (...checks) => obj => (
    checks.reduce((acc, type) => {
        if (!checker_module[type](obj)) {
            acc = false;
        }
        return acc;
    }, true)
);
