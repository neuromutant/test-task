// this one is abstraction magic that I use with Vuex not to write models in components,
// but in store itself.
// I got used to this code, but now there are better Vuex alternatives such as Pinia

import _get                       from 'lodash.get';
import store                      from '../store/store';

const toGetSet = (path, method, property) => {
    return {
        get() {
            return _get(store['_modulesNamespaceMap'][path].state, property);
        },
        set(val) {
            return method(val);
        },
    };
};

export const mapModels = <T>(path_to_store, models:T, pickedModels:Array<keyof T>) => {
    const res = Object.entries(models).reduce((acc, [model_name, val])  => {
        if (!pickedModels.includes(model_name as keyof T)) return acc;

        const [stateProp, mutation] = val;

        const commitFun = mutationVal => {
            store.commit(path_to_store + '/' + mutation, mutationVal);
        };
        
        const getterAndSetter = toGetSet(path_to_store + '/', commitFun, stateProp); 
        acc[model_name]       = getterAndSetter;
        return acc;
    }, {});
    return res;
};