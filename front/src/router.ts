//import { PermissionTypes }                  from '@client';
import qs                   from 'qs';
import Vue                  from 'vue';
import Router               from 'vue-router';
import { MetricsRoutes }    from './entities/Metrics/routes/metrics.routes';
import { UserRoutes }       from './entities/Users/routes/users.routes';

Vue.use(Router);

export const routes = [
    {
        path     : '/',
        component: () => import('./views/Welcome.vue'),
    },
    ...UserRoutes,
    ...MetricsRoutes
];

const isDate = function(date) {
    const parsedDate = Date.parse(date);
    // You want to check again for !isNaN(parsedDate) here because Dates can be converted
    // to numbers, but a failed Date parse will not.
    if (isNaN(date) && !isNaN(parsedDate)) {
        return true;
    }
};

export const router = new Router({
    mode: 'history',
    base: import.meta.env.BASE_URL,
    routes,
    parseQuery(query) {
        // custom front query params stringifier
        return qs.parse(query, {
            decoder(str) {
                if (str === 'false') {
                    return false;
                }
                if (str === 'true') {
                    return true;
                }
                if (isDate(str)) {
                    return decodeURI(str);
                }

                if (!isNaN(parseInt(str))) {
                    return parseInt(str);
                }

                return decodeURI(str);
            },
        });
    },
    stringifyQuery(query) {
        const result = qs.stringify(query, {
            encode: false,
        });
        return result
            ? '?' + result
            : '';
    },
});

router.beforeEach(async(to, from, next) => {
    return next();
});

const originalPush    = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};
