import Vue                                              from 'vue';
import App                                              from './App.vue';
import {router}                                         from './router';
import store                                            from './store/store';
import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css'; 

import Vuetify         from 'vuetify/lib';
import * as components from 'vuetify/lib/components';
import * as directives from 'vuetify/lib/directives';

Vue.use(Vuetify, {components, directives});

const vuetify = new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    data() {
        return {
            windowWidth: window.innerWidth,
        };
    },
    mounted() {
        window.onresize = () => {
            this.windowWidth = window.innerWidth;
        };
    },
    render: h => h(App),
}).$mount('#app');

