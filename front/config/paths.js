const paths = [
    { alias: "@", path: "src" },
    { alias: "@api", path: "src/api/api.ts" },
    { alias: "@entities", path: "src/entities" },
    { alias: "@components", path: "src/components" },
    // this one is used in case any plugin relies on vue template compiler.
    { alias: "vue", path: "node_modules/vue/dist/vue.esm.js" },
]

module.exports = paths;