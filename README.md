# Backend

## Installation
```bash
$ cd back
$ docker-compose-up -d
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

# Frontend

## Installation
```bash
$ cd front
$ npm install
```

## Running the app

```bash
# development
$ npm run sertve
```  
open http://localhost:40011/admin/